﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegeta
{
    internal class Student
    {

        private delegate String getString();
        private delegate int getInt ();
        private delegate DateTime getDateBirthDate();
        private int _id;

        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private DateTime _date;

        public DateTime DateBirth
        {
            get { return _date; }
            set { _date = value; }
        }
        private string _gender;

        public string Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }
        public string getName() { 
            return Name;
        }
        public int getID() { 
            return ID;
        }
        public string getGender() { 
            return Gender;
        }
        public DateTime getDateBirth() { 
            return DateBirth;
        }

        public void ShowAll() {
            getString Name = new getString(getName);
            getInt ID = new getInt(getID);
            getDateBirthDate Date = new getDateBirthDate(getDateBirth);
            getString gender = new getString(getGender);

            var final = Name() + ID() + Date() + gender();
            Console.WriteLine(final);
           
        
        }

    }
}
